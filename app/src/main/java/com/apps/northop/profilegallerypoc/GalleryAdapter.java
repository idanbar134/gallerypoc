package com.apps.northop.profilegallerypoc;

import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.draglistview.DragItemAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by idan on 01/02/2017.
 * Show a list of photo's and have a state of deleting them.
 */

public class GalleryAdapter extends DragItemAdapter<String, GalleryAdapter.ViewHolder> {

  private int mGrabHandleId;
  private boolean mDragOnLongPress;

  private int mLayoutId;
  private List<String> photos = new ArrayList<>();
  private Vibrator vibrator;

  public GalleryAdapter(List<String> fakeData, int layoutId, int grabHandleId,
      boolean dragOnLongPress) {
    mLayoutId = layoutId;
    mGrabHandleId = grabHandleId;
    mDragOnLongPress = dragOnLongPress;
    setHasStableIds(true);
    setItemList(fakeData);
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return new ViewHolder(
        LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_item, parent, false));
  }

  @Override public void onBindViewHolder(GalleryAdapter.ViewHolder holder, int position) {
    super.onBindViewHolder(holder, position);
    String photo = mItemList.get(position);

    Glide.with(holder.itemView.getContext()).
        load(photo).
        into(holder.photo);

    holder.header.setVisibility(position == 0 ? View.VISIBLE : View.INVISIBLE);
        /*holder.itemView.setOnLongClickListener(v ->

        {
            vibrator = (Vibrator) v.getContext().getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(50);

            return false;
        });*/
  }

  @Override public long getItemId(int position) {
    return mItemList.get(position).hashCode();
  }

  class ViewHolder extends DragItemAdapter.ViewHolder /*implements ItemTouchHelperViewHolder */ {

    @BindView(R.id.gallery_photo) ImageView photo;
    @BindView(R.id.profile_header) TextView header;

    ViewHolder(View itemView) {
      super(itemView, mGrabHandleId, mDragOnLongPress);
      ButterKnife.bind(this, itemView);
    }

        /*@Override
        public void onItemSelected() {
            photo.setAlpha(0.7f);
        }

        @Override
        public void onItemClear() {
            photo.setAlpha(1f);
        }*/
  }
}