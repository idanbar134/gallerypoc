package com.apps.northop.profilegallerypoc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.draglistview.CustomGridLayoutManager;
import com.draglistview.DragRecyclerView;
import java.util.ArrayList;
import java.util.List;

public class GalleryActivity extends AppCompatActivity {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.drag_list_view) DragRecyclerView mDragRecyclerView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_gallery);
    ButterKnife.bind(this);
    initToolbar();
    setupGridVerticalRecyclerView();
  }

  private void setupGridVerticalRecyclerView() {

    CustomGridLayoutManager customGridLayoutManager =
        new CustomGridLayoutManager(new CustomGridLayoutManager.GridSpanLookup() {
          @Override public CustomGridLayoutManager.SpanInfo getSpanInfo(int position) {
            // Conditions for 2x2 items
            if (position == 0) {
              return new CustomGridLayoutManager.SpanInfo(2, 2);
            } else {
              return new CustomGridLayoutManager.SpanInfo(1, 1);
            }
          }
        }, 3, 1f);

    mDragRecyclerView.getRecyclerView().setVerticalScrollBarEnabled(true);
    mDragRecyclerView.setLayoutManager(customGridLayoutManager);
    GalleryAdapter listAdapter =
        new GalleryAdapter(getFakeData(), R.layout.gallery_item, R.id.container, true);
    mDragRecyclerView.setAdapter(listAdapter, true);
    mDragRecyclerView.setCanDragHorizontally(true);
    mDragRecyclerView.setCustomDragItem(null);
  }

  private void initToolbar() {
    setSupportActionBar(toolbar);
  }

  private List<String> getFakeData() {
    ArrayList<String> list = new ArrayList<>();
    list.add(
        "https://ae01.alicdn.com/kf/HTB1bMtGRXXXXXa3XpXXq6xXFXXX9/YEAPOOK-Striped-Skirt-One-Piece-Swimsuit-2017-Sexy-Swimwear-font-b-Women-b-font-font-b.jpg");
    list.add("https://static.pexels.com/photos/213117/pexels-photo-213117.jpeg");
    list.add(
        "https://ae01.alicdn.com/kf/HTB1TrVSKFXXXXXfXpXXq6xXFXXXK/Free-Shipping-10PCS-LOT-Sexy-Women-s-Small-Vest-Shorts-T-Shirts-Wrapped-Chest-Nightclub-Shirts.jpg");
    list.add(
        "https://ae01.alicdn.com/kf/HTB1t28oIXXXXXcIXXXXq6xXFXXXM/Fashion-Women-Nightwear-Small-Clothes-Cross-Shoulder-Strap-Racerback-Sexy-Sleepwear-Ladies-Sexy-Nightwear-Pajama-SV003638.jpg");
    list.add(
        "http://www.dhresource.com/260x260s/f2-albu-g5-M00-12-B4-rBVaJFgv4HmADzsdAAP2MSW9Gkg771.jpg/the-autumn-winter-new-ladies-sexy-long-sleeved.jpg");
    list.add("https://images-na.ssl-images-amazon.com/images/I/61Dy3Xa97BL._UY445_.jpg");
    list.add(
        "https://s-media-cache-ak0.pinimg.com/236x/f7/93/06/f79306ef9cf634334e79b2ebf1c89473--halloween-costumes-online-costume-halloween.jpg");
    return list;
  }
}

