package com.apps.northop.profilegallerypoc;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Admin on 20-08-2017.
 */

public class SquareImageView extends android.support.v7.widget.AppCompatImageView {

    private int dominantSide;

    public SquareImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SquareImageView);
        dominantSide = a.getInt(R.styleable.SquareImageView_dominantSide, 0);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int edgeLen = dominantSide == 0 ? getMeasuredWidth() : getMeasuredHeight();
        setMeasuredDimension(edgeLen, edgeLen);
    }
}
