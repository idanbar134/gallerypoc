package com.draglistview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

public class DragItemRecyclerView extends RecyclerView implements AutoScroller.AutoScrollListener {

    public interface DragItemListener {
        void onDragStarted(int itemPosition, float x, float y);

        void onDragging(int itemPosition, float x, float y);

        void onDragEnded(int newItemPosition);
    }

    public interface DragItemCallback {
        boolean canDragItemAtPosition(int dragPosition);

        boolean canDropItemAtPosition(int dropPosition);
    }

    private enum DragState {
        DRAG_STARTED, DRAGGING, DRAG_ENDED
    }

    private AutoScroller mAutoScroller;
    private DragItemListener mListener;
    private DragItemCallback mDragCallback;
    private DragState mDragState = DragState.DRAG_ENDED;
    private DragItemAdapter mAdapter;
    private DragItem mDragItem;
    private Drawable mDropTargetBackgroundDrawable;
    private Drawable mDropTargetForegroundDrawable;
    private long mDragItemId = NO_ID;
    private boolean mHoldChangePosition;
    private int mDragItemPosition;
    private int mTouchSlop;
    private float mStartY;
    private boolean mClipToPadding;
    private boolean mCanNotDragAboveTop;
    private boolean mCanNotDragBelowBottom;
    private boolean mScrollingEnabled = true;
    private boolean mDisableReorderWhenDragging;
    private boolean mDragEnabled = true;

    public DragItemRecyclerView(Context context) {
        super(context);
        init();
    }

    public DragItemRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
        init();
    }

    public DragItemRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        mAutoScroller = new AutoScroller(getContext(), this);
        mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();

        addItemDecoration(new ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, State state) {
                super.onDraw(c, parent, state);
                drawDecoration(c, parent, mDropTargetBackgroundDrawable);
            }

            @Override
            public void onDrawOver(Canvas c, RecyclerView parent, State state) {
                super.onDrawOver(c, parent, state);
                drawDecoration(c, parent, mDropTargetForegroundDrawable);
            }

            private void drawDecoration(Canvas c, RecyclerView parent, Drawable drawable) {
                if (mAdapter == null || mAdapter.getDropTargetId() == NO_ID || drawable == null) {
                    return;
                }

                for (int i = 0; i < parent.getChildCount(); i++) {
                    View item = parent.getChildAt(i);
                    int pos = getChildAdapterPosition(item);
                    if (pos != NO_POSITION && mAdapter.getItemId(pos) == mAdapter.getDropTargetId()) {
                        drawable.setBounds(item.getLeft(), item.getTop(), item.getRight(), item.getBottom());
                        drawable.draw(c);
                    }
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!mScrollingEnabled) {
            return false;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mStartY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                final float diffY = Math.abs(event.getY() - mStartY);
                if (diffY > mTouchSlop * 0.5) {
                    // Steal event from parent as we now only want to scroll in the list
                    getParent().requestDisallowInterceptTouchEvent(true);
                }
                break;
        }
        return super.onInterceptTouchEvent(event);
    }

    void setDragEnabled(boolean enabled) {
        mDragEnabled = enabled;
    }

    boolean isDragEnabled() {
        return mDragEnabled;
    }

    void setDragItemListener(DragItemListener listener) {
        mListener = listener;
    }

    void setDragItemCallback(DragItemCallback callback) {
        mDragCallback = callback;
    }

    void setDragItem(DragItem dragItem) {
        mDragItem = dragItem;
    }

    boolean isDragging() {
        return mDragState != DragState.DRAG_ENDED;
    }

    long getDragItemId() {
        return mDragItemId;
    }

    @Override
    public void setClipToPadding(boolean clipToPadding) {
        super.setClipToPadding(clipToPadding);
        mClipToPadding = clipToPadding;
    }

    @Override
    public void setAdapter(Adapter adapter) {
        if (!isInEditMode()) {
            if (!(adapter instanceof DragItemAdapter)) {
                throw new RuntimeException("Adapter must extend DragItemAdapter");
            }
            if (!adapter.hasStableIds()) {
                throw new RuntimeException("Adapter must have stable ids");
            }
        }

        super.setAdapter(adapter);
        mAdapter = (DragItemAdapter) adapter;
    }

    @Override
    public void setLayoutManager(LayoutManager layout) {
        super.setLayoutManager(layout);
    }

    @Override
    public void onAutoScrollPositionBy(int dx, int dy) {
        if (isDragging()) {
            scrollBy(dx, dy);
            updateDragPositionAndScroll();
        } else {
            mAutoScroller.stopAutoScroll();
        }
    }

    @Override
    public void onAutoScrollColumnBy(int columns) {
    }

    private View findChildView(float x, float y) {
        final int count = getChildCount();
        if (y <= 0 && count > 0) {
            return getChildAt(0);
        }

        for (int i = count - 1; i >= 0; i--) {
            final View child = getChildAt(i);
            MarginLayoutParams params = (MarginLayoutParams) child.getLayoutParams();
            if (x >= child.getLeft() - params.leftMargin && x <= child.getRight() + params.rightMargin
                    && y >= child.getTop() - params.topMargin && y <= child.getBottom() + params.bottomMargin) {
                return child;
            }
        }

        return null;
    }

    private boolean shouldChangeItemPosition(int newPos) {
        // Check if drag position is changed and valid and that we are not in a hold position state
        if (mHoldChangePosition || mDragItemPosition == NO_POSITION || mDragItemPosition == newPos) {
            return false;
        }
        // If we are not allowed to drag above top or bottom and new pos is 0 or item count then return false
        if ((mCanNotDragAboveTop && newPos == 0) || (mCanNotDragBelowBottom && newPos == mAdapter.getItemCount() - 1)) {
            return false;
        }
        // Check with callback if we are allowed to drop at this position
        if (mDragCallback != null && !mDragCallback.canDropItemAtPosition(newPos)) {
            return false;
        }
        return true;
    }

    private void updateDragPositionAndScroll() {
        View view = findChildView(mDragItem.getX(), mDragItem.getY());
        int newPos = getChildLayoutPosition(view);
        if (newPos == NO_POSITION || view == null) {
            return;
        }

        // If using a LinearLayoutManager and the new view has a bigger height we need to check if passing centerY as well.
        // If not doing this extra check the bigger item will move back again when dragging slowly over it.
        MarginLayoutParams params = (MarginLayoutParams) view.getLayoutParams();
        int viewHeight = view.getMeasuredHeight() + params.topMargin + params.bottomMargin;
        int viewCenterY = view.getTop() - params.topMargin + viewHeight / 2;
        boolean dragDown = mDragItemPosition < getChildLayoutPosition(view);
        boolean movedPassedCenterY = dragDown ? mDragItem.getY() > viewCenterY : mDragItem.getY() < viewCenterY;

        // If new height is bigger then current and not passed centerY then reset back to current position
        if (viewHeight > mDragItem.getDragItemView().getMeasuredHeight() && !movedPassedCenterY) {
            newPos = mDragItemPosition;
        }

        CustomGridLayoutManager layoutManager = (CustomGridLayoutManager) getLayoutManager();
        if (shouldChangeItemPosition(newPos)) {
            if (mDisableReorderWhenDragging) {
                mAdapter.setDropTargetId(mAdapter.getItemId(newPos));
                mAdapter.notifyDataSetChanged();
            } else {
                int pos = layoutManager.getFirstVisibleItemPosition();
                mAdapter.changeItemPosition(mDragItemPosition, newPos);
                mDragItemPosition = newPos;

                // Since notifyItemMoved scrolls the list we need to scroll back to where we were after the position change.
                layoutManager.scrollToPosition(pos);
            }
        }

        boolean lastItemReached = false;
        boolean firstItemReached = false;
        int top = mClipToPadding ? getPaddingTop() : 0;
        int bottom = mClipToPadding ? getHeight() - getPaddingBottom() : getHeight();
        int left = mClipToPadding ? getPaddingLeft() : 0;
        int right = mClipToPadding ? getWidth() - getPaddingRight() : getWidth();
        ViewHolder lastChild = findViewHolderForLayoutPosition(mAdapter.getItemCount() - 1);
        ViewHolder firstChild = findViewHolderForLayoutPosition(0);

        // Check if first or last item has been reached
        if (lastChild != null && lastChild.itemView.getBottom() <= bottom) {
            lastItemReached = true;
        }
        if (firstChild != null && firstChild.itemView.getTop() >= top) {
            firstItemReached = true;
        }
        // Start auto scroll if at the edge
        if (mDragItem.getY() > getHeight() - view.getHeight() / 2 && !lastItemReached) {
            mAutoScroller.startAutoScroll(AutoScroller.ScrollDirection.UP);
        } else if (mDragItem.getY() < view.getHeight() / 2 && !firstItemReached) {
            mAutoScroller.startAutoScroll(AutoScroller.ScrollDirection.DOWN);
        } else {
            mAutoScroller.stopAutoScroll();
        }
    }

    boolean startDrag(View itemView, long itemId, float x, float y) {
        int dragItemPosition = mAdapter.getPositionForItemId(itemId);
        if (!mDragEnabled || (mCanNotDragAboveTop && dragItemPosition == 0)
                || (mCanNotDragBelowBottom && dragItemPosition == mAdapter.getItemCount() - 1)) {
            return false;
        }

        if (mDragCallback != null && !mDragCallback.canDragItemAtPosition(dragItemPosition)) {
            return false;
        }

        // If a drag is starting the parent must always be allowed to intercept
        getParent().requestDisallowInterceptTouchEvent(false);
        mDragState = DragState.DRAG_STARTED;
        mDragItemId = itemId;
        mDragItem.startDrag(itemView, x, y);
        mDragItemPosition = dragItemPosition;
        updateDragPositionAndScroll();

        mAdapter.setDragItemId(mDragItemId);
        mAdapter.notifyDataSetChanged();
        if (mListener != null) {
            mListener.onDragStarted(mDragItemPosition, mDragItem.getX(), mDragItem.getY());
        }

        invalidate();
        return true;
    }

    void onDragging(float x, float y) {
        if (mDragState == DragState.DRAG_ENDED) {
            return;
        }

        mDragState = DragState.DRAGGING;
        mDragItemPosition = mAdapter.getPositionForItemId(mDragItemId);
        mDragItem.setPosition(x, y);

        if (!mAutoScroller.isAutoScrolling()) {
            updateDragPositionAndScroll();
        }

        if (mListener != null) {
            mListener.onDragging(mDragItemPosition, x, y);
        }
        invalidate();
    }

    void onDragEnded() {
        // Need check because sometimes the framework calls drag end twice in a row
        if (mDragState == DragState.DRAG_ENDED) {
            return;
        }

        mAutoScroller.stopAutoScroll();
        setEnabled(false);

        if (mDisableReorderWhenDragging) {
            int newPos = mAdapter.getPositionForItemId(mAdapter.getDropTargetId());
            if (newPos != NO_POSITION) {
                mAdapter.swapItems(mDragItemPosition, newPos);
                mDragItemPosition = newPos;
            }
            mAdapter.setDropTargetId(NO_ID);
        }

        // Post so layout is done before we start end animation
        post(new Runnable() {
            @Override
            public void run() {
                // Sometimes the holder will be null if a holder has not yet been set for the position
                final RecyclerView.ViewHolder holder = findViewHolderForAdapterPosition(mDragItemPosition);
                if (holder != null) {
                    getItemAnimator().endAnimation(holder);
                    mDragItem.endDrag(holder.itemView, new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            holder.itemView.setAlpha(1);
                            onDragItemAnimationEnd();
                        }
                    });
                } else {
                    onDragItemAnimationEnd();
                }
            }
        });
    }

    private void onDragItemAnimationEnd() {
        mAdapter.setDragItemId(NO_ID);
        mAdapter.setDropTargetId(NO_ID);
        mAdapter.notifyDataSetChanged();

        mDragState = DragState.DRAG_ENDED;
        if (mListener != null) {
            mListener.onDragEnded(mDragItemPosition);
        }

        mDragItemId = NO_ID;
        mDragItem.hide();
        setEnabled(true);
        invalidate();
    }
}
